FROM python:3.6
ENV PYTHONUNBUFFERED 1

WORKDIR /code
COPY . /code/
RUN python setup.py install

VOLUME /caduceus
WORKDIR /caduceus

CMD caduceus -i 0.0.0.0 -p 5000
